﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    List<Goombas> goombas;

    int CountDead()
    {
        int count = 0;

        for (int i = 0; i < goombas.Count; i++)
        {
            if (goombas[i].IsAlive() == false)
            {
                count++;
            }
        }

        return count;

    }
}
