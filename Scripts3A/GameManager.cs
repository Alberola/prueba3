﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    List<Combat> combats;

    int CountCombats(string name)
    {
        int count = 0;

        for(int i = 0; i < combats.Count; i++)
        {
            if(combats[i].pokemon1 == name || combats[i].pokemon2 == name)
            {
                count++;
            }
        }

        return count;
    }

}
